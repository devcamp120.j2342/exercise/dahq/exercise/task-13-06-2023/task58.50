package com.devcamp.employeeapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.employeeapi.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
